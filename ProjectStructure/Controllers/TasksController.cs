﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.Models;
using ProjectStructure.Services.Abstraction;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ITasksService tasksService;

        public TasksController(ITasksService tasksService)
        {
            this.tasksService = tasksService;
        }
        [HttpGet]
        public List<Task> GetTasks()
        {
            return tasksService.GetTasks();
        }

        [HttpGet("NotFinishedTasks/{id}")]
        public List<Task> GetNoFinishedTasks([FromRoute]int id)
        {
            return tasksService.GetNoFinishedTasks(id);
        }

        [HttpGet("{id}")]
        public Task GetTaskId([FromRoute] int id)
        {
            return tasksService.GetTaskId(id);
        }
        [HttpPost]
        public ActionResult<Task> CreateTask([FromBody] TaskDto task)
        {
            try
            {
                return Ok(tasksService.CreateTask(task));
            }
            catch (ArgumentNullException exception)
            {
                return BadRequest(exception);
            }
            catch (Exception exception)
            {
                return StatusCode(500, exception);
            }
        }
        [HttpDelete("{id}")]
        public void DeleteTask([FromRoute] int id)
        {
            tasksService.DeleteTask(id);
        }
        [HttpPut("{id}")]
        public void UpdateTask([FromRoute] int id, [FromBody] TaskDto task)
        {
            tasksService.UpdateTask(id, task);
        }
    }
}
