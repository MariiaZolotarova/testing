﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.Models;
using ProjectStructure.Services.Abstraction;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CollectionController : ControllerBase
    {
        private readonly ICollectionService collectionService;
        public CollectionController(ICollectionService collectionService)
        {
            this.collectionService = collectionService;
        }

        [HttpGet("ShowCountProjectsWithTasks/{userId}")]
        public Dictionary<string, int> GetProjects([FromRoute] int userId)
        {
            return collectionService.ShowCountProjectsWithTasks(userId);
        }

        [HttpGet("ShowDesignedTasksForSpecialUser/{userId}")]
        public List<Task> ShowDesignedTasksForSpecialUser([FromRoute] int userId)
        {
            return collectionService.ShowDesignedTasksForSpecialUser(userId);
        }

        [HttpGet("ShowPerformedTasksInCurrentYear/{userId}")]
        public List<NameId> ShowPerformedTasksInCurrentYear([FromRoute] int userId)
        {
            return collectionService.ShowPerformedTasksInCurrentYear(userId);
        }

        [HttpGet("ShowListOfTeamsOlderThan")]
        public List<TeamUsers> ShowListOfTeamsOlderThan()
        {
            return collectionService.ShowListOfTeamsOlderThan();
        }

        [HttpGet("ShowListOfUsersWithSortedTasks")]
        public List<UserTasks> ShowListOfUsersWithSortedTasks()
        {
            return collectionService.ShowListOfUsersWithSortedTasks();
        }

        [HttpGet("StructOfUser/{userId}")]
        public ProjectTasks StructOfUser([FromRoute] int userId)
        {
            return collectionService.StructOfUser(userId);
        }

        [HttpGet("StructOfProjects")]
        public List<ProjectInfo> StructOfProjects()
        {
            return collectionService.StructOfProjects();
        }
    }
}
