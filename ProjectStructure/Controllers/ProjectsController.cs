﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.Models;
using ProjectStructure.Services.Abstraction;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {

        private readonly IProjectsService projectsService;

        public ProjectsController(IProjectsService projectsService)
        {
            this.projectsService = projectsService;
        }
        
        [HttpGet]
        public List<Project> GetProjects()
        {
            return projectsService.GetProjects();
        }

        [HttpGet("{id}")]
        public Project GetProjectsId([FromRoute]int id)
        {
            return projectsService.GetProjectId(id);
        }

        [HttpPost]
        public ActionResult<Project> CreateProject([FromBody] ProjectDto project)
        {
            try
            {
                return Ok(projectsService.CreateProject(project));
            }
            catch (ArgumentNullException exception)
            {
                return BadRequest(exception);
            }
            catch (Exception exception)
            {
                return StatusCode(500, exception);
            }
        }

        [HttpDelete("{id}")]
        public void DeleteProject([FromRoute] int id)
        {
            projectsService.DeleteProject(id);
        }

        [HttpPut("{id}")]
        public void UpdateProject([FromRoute] int id, [FromBody] ProjectDto project)
        {
            projectsService.UpdateProject(id, project);
        }
    }
}
