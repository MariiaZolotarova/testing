﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.Models;
using ProjectStructure.Services.Abstraction;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamsService teamsService;

        public TeamsController(ITeamsService teamsService)
        {
            this.teamsService = teamsService;
        }
        [HttpGet]
        public List<Team> GetTeams()
        {
            return teamsService.GetTeams();
        }
        [HttpGet("{id}")]
        public Team GetTeamsId([FromRoute] int id)
        {
            return teamsService.GetTeamId(id);
        }

        [HttpPost]
        public ActionResult<Team> CreateTeam([FromBody] TeamDto team)
        {
            try
            {
                return Ok(teamsService.CreateTeam(team));
            }
            catch (ArgumentNullException exception)
            {
                return BadRequest(exception);
            }
            catch (Exception exception)
            {
                return StatusCode(500, exception);
            }

        }

        [HttpDelete("{id}")]
        public void DeleteTeam([FromRoute] int id)
        {
            teamsService.DeleteTeam(id);
        }

        [HttpPut("{id}")]
        public void UpdateTeam([FromRoute] int id, [FromBody] TeamDto team)
        {
            teamsService.UpdateTeam(id, team);
        }
    }
}
