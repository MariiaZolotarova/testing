﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.BL.UnitOfWork;
using ProjectStructure.Models;
using ProjectStructure.Services.Abstraction;

namespace ProjectStructure.Services.Implementation
{
    public class UsersService : IUsersService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public UsersService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public List<User> GetUsers()
        {
            var userRepository = unitOfWork.Set<User>();
            return userRepository.Get().ToList();
        }
       
        public User GetUsersId(int id)
        {
            var userRepository = unitOfWork.Set<User>();
            return userRepository.Get(x => x.Id == id).FirstOrDefault();
        }

        public User CreateUser(UserDto userDto)
        {
            if (userDto == null || string.IsNullOrWhiteSpace(userDto.FirstName) ||
                string.IsNullOrWhiteSpace(userDto.LastName))
            {
                throw new ArgumentNullException();
            }

            var userRepository = unitOfWork.Set<User>();
            var user = mapper.Map<User>(userDto);
            user.RegisteredAt = DateTime.UtcNow;

            userRepository.Create(user);
            unitOfWork.SaveChanges();
            return user;
        }

        public void DeleteUser(int id)
        {
            var userRepository = unitOfWork.Set<User>();

            userRepository.Delete(id);
            unitOfWork.SaveChanges();
        }
        public void UpdateUser(int id, UserDto userDto)
        {
            var userRepository = unitOfWork.Set<User>();
            var user = mapper.Map<User>(userDto);
            user.Id = id;

            userRepository.Update(user);
            unitOfWork.SaveChanges();
        }
    }
}
