﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.BL.UnitOfWork;
using ProjectStructure.Models;
using ProjectStructure.Services.Abstraction;

namespace ProjectStructure.Services.Implementation
{
    public class TasksService : ITasksService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        public TasksService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public List<Task> GetTasks()
        {
            var taskRepository = unitOfWork.Set<Task>();
            return taskRepository.Get().ToList();
        }

        public List<Task> GetNoFinishedTasks(int id)
        {
            var taskRepository = unitOfWork.Set<Task>();
            return taskRepository.Get(x => x.PerformerId == id && x.State != StateEnum.Finished).ToList();
        }

        public Task GetTaskId(int id)
        {
            var taskRepository = unitOfWork.Set<Task>();
            return taskRepository.Get(x => x.Id == id).FirstOrDefault();
        }
        public Task CreateTask(TaskDto taskDto)
        {
            if (taskDto == null || string.IsNullOrWhiteSpace(taskDto.Name))
            {
                throw new ArgumentNullException();
            }

            var taskRepository = unitOfWork.Set<Task>();
            var task = mapper.Map<Task>(taskDto);
            task.CreatedAt = DateTime.UtcNow;

            if (task.State == StateEnum.Finished)
            {
                task.FinishedAt = DateTime.UtcNow;
            }
            else
            {
                task.FinishedAt = null;
            }

            taskRepository.Create(task);
            unitOfWork.SaveChanges();
            return task;
        }

        public void DeleteTask(int id)
        {
            var taskRepository = unitOfWork.Set<Task>();

            taskRepository.Delete(id);
            unitOfWork.SaveChanges();
        }

        public void UpdateTask(int id, TaskDto taskDto)
        {
            var taskRepository = unitOfWork.Set<Task>();
            var task = mapper.Map<Task>(taskDto);
            task.Id = id;


            if (task.State == StateEnum.Finished)
            {
                task.FinishedAt = DateTime.UtcNow;
            }
            else
            {
                task.FinishedAt = null;
            }

            taskRepository.Update(task);
            unitOfWork.SaveChanges();
        }
    }
}
