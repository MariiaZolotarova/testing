﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.BL.UnitOfWork;
using ProjectStructure.Models;
using ProjectStructure.Services.Abstraction;

namespace ProjectStructure.Services.Implementation
{
    public class TeamsService : ITeamsService
    {
        private readonly IMapper mapper;
        private readonly IUnitOfWork unitOfWork;

        public TeamsService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public List<Team> GetTeams()
        {
            var teamRepository = unitOfWork.Set<Team>();
            return teamRepository.Get().ToList();
        }

        public Team GetTeamId(int id)
        {
            var teamRepository = unitOfWork.Set<Team>();
            return teamRepository.Get(x => x.Id == id).FirstOrDefault();
        }

        public Team CreateTeam(TeamDto teamDto)
        {
            if (teamDto == null || string.IsNullOrWhiteSpace(teamDto.Name))
            {
                throw new ArgumentNullException();
            }


            var teamRepository = unitOfWork.Set<Team>();
            var team = mapper.Map<Team>(teamDto);
            team.CreatedAt = DateTime.UtcNow;

            teamRepository.Create(team);
            unitOfWork.SaveChanges();
            return team;
        }
        public void DeleteTeam(int id)
        {
            var teamRepository = unitOfWork.Set<Team>();

            teamRepository.Delete(id);
            unitOfWork.SaveChanges();
        }

        public void UpdateTeam(int id, TeamDto teamDto)
        {
            var teamRepository = unitOfWork.Set<Team>();
            var team = mapper.Map<Team>(teamDto);
            team.Id = id;

            teamRepository.Update(team);
            unitOfWork.SaveChanges();
        }

    }
}
