﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.BL.UnitOfWork;
using ProjectStructure.Models;
using ProjectStructure.Services.Abstraction;
using Task = ProjectStructure.BL.Context.Entity.Task;

namespace ProjectStructure.Services.Implementation
{
    public class CollectionService : ICollectionService
    {
        private readonly IUnitOfWork unitOfWork;

        public CollectionService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public Dictionary<string, int> ShowCountProjectsWithTasks(int userId)
        {
            var projects = GetProjects();
            return projects.Where(x => x.AuthorId == userId).ToDictionary(x => x.Name, x => x.Tasks.Count);
        }

        public List<Task> ShowDesignedTasksForSpecialUser(int userId)
        {
            var projects = GetProjects();
            int lengthName = 45;

            return projects.Where(x => x.AuthorId == userId)
                .SelectMany(x => x.Tasks)
                .Where(x => x.Name.Length < lengthName && x.PerformerId == userId)
                .ToList();
        }

        public List<NameId> ShowPerformedTasksInCurrentYear(int userId)
        {
            var projects = GetProjects();
            return projects.Where(x => x.AuthorId == userId)
                .SelectMany(x => x.Tasks)
                .Where(x => x.FinishedAt.GetValueOrDefault().Year == DateTime.UtcNow.Year
                            && x.State == StateEnum.Finished
                            && x.PerformerId == userId)
                .Select(x => new NameId
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList();
        }

        public List<TeamUsers> ShowListOfTeamsOlderThan()
        {
            var projects = GetProjects();
            return projects
                .GroupBy(x => new
                {
                    x.TeamId
                })
                .Select(x => new TeamUsers
                {
                    TeamId = x.Key.TeamId,
                    Name = x.First().Name,
                    Users = x.First().Team.Users.Where(x => x.Birthday.Year <= DateTime.UtcNow.Year - 10).OrderByDescending(y => y.RegisteredAt).ToList()
                }).ToList();
        }

        public List<UserTasks> ShowListOfUsersWithSortedTasks()
        {
            var projects = GetProjects();
            return projects.SelectMany(x => x.Tasks).GroupBy(x => x.PerformerId).Select(x => new UserTasks
            {
                User = x.First(c => c.PerformerId == x.Key).Performer,
                Tasks = x.OrderByDescending(m => m.Name.Length).ToList()
            }).OrderBy(v => v.User.FirstName).ToList();
        }

        public ProjectTasks StructOfUser(int userId)
        {
            var projects = GetProjects();
            var projectTasks = projects.Where(x => x.AuthorId == userId)
                .Select(x => new ProjectTasks
                {
                    User = x.Author,
                    Project = x,
                    TasksCount = x.Tasks.Count(x => x.PerformerId == userId),
                    NotFinishedTasks = x.Tasks.Count(x => x.PerformerId == userId && x.State != StateEnum.Finished),
                    LongestTask = x.Tasks.Any() ? x.Tasks.Aggregate((i1, i2) => (i1.FinishedAt.GetValueOrDefault().Ticks - i1.CreatedAt.Ticks) > (i2.FinishedAt.GetValueOrDefault().Ticks - i2.CreatedAt.Ticks) ? i1 : i2) : null
                }).OrderByDescending(x => x.Project.CreatedAt).FirstOrDefault();

            return projectTasks;
        }

        public List<ProjectInfo> StructOfProjects()
        {
            var projects = GetProjects();
            return projects.Select(x => new ProjectInfo
            {
                Project = x,
                LongestTask = x.Tasks.Any() ? x.Tasks.Aggregate((i1, i2) => i1.Description.Length > i2.Description.Length ? i1 : i2) : null,
                ShortestTask = x.Tasks.Any() ? x.Tasks.Aggregate((i1, i2) => i1.Name.Length < i2.Name.Length ? i1 : i2) : null,
                UsersCount = x.Description.Length > 20 || x.Tasks.Count < 3 ? x.Team.Users.Count : 0
            }).ToList();
        }

        private List<Project> GetProjects()
        {
            var projects = unitOfWork.Set<Project>().Get().ToList();
            var tasks = unitOfWork.Set<Task>().Get().ToList();
            var users = unitOfWork.Set<User>().Get().ToList();
            var teams = unitOfWork.Set<Team>().Get().ToList();

            var joinedProjects = projects.Join(users, project => project.AuthorId, user => user.Id, (project, user) =>
            {
                project.Author = user;
                return project;
            }).Join(teams, project => project.TeamId, team => team.Id, (project, team) =>
            {
                team.Users = users.Where(x => x.TeamId == team.Id).ToList();
                project.Team = team;
                return project;
            }).GroupJoin(tasks, project => project.Id, task => task.ProjectId, (project, task) =>
            {
                project.Tasks = task.Join(users, task => task.PerformerId, user => user.Id, (task, user) =>
                {
                    task.Performer = user;
                    return task;
                }).ToList();
                return project;
            }).ToList();

            return joinedProjects;
        }
    }
}
