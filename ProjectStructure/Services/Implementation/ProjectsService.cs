﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.BL.UnitOfWork;
using ProjectStructure.Models;
using ProjectStructure.Services.Abstraction;

namespace ProjectStructure.Services.Implementation
{
    public class ProjectsService : IProjectsService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        public ProjectsService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public List<Project> GetProjects()
        {
            var projectRepository = unitOfWork.Set<Project>();
            return projectRepository.Get().ToList();
        }

        public Project GetProjectId(int id)
        {
            var projectRepository = unitOfWork.Set<Project>();
            return projectRepository.Get(x => x.Id == id).FirstOrDefault();
        }
        public Project CreateProject(ProjectDto projectDto)
        {
            if (projectDto == null || String.IsNullOrWhiteSpace(projectDto.Name))
            {
                throw new ArgumentNullException();
            }

            var projectRepository = unitOfWork.Set<Project>();
            var project = mapper.Map<Project>(projectDto);
            project.CreatedAt = DateTime.UtcNow;

            projectRepository.Create(project);
            unitOfWork.SaveChanges();

            return project;
        }
        public void DeleteProject(int id)
        {
            var projectRepository = unitOfWork.Set<Project>();

            projectRepository.Delete(id);
            unitOfWork.SaveChanges();
        }
        public void UpdateProject(int id, ProjectDto projectDto)
        {
            var projectRepository = unitOfWork.Set<Project>();
            var project = mapper.Map<Project>(projectDto);
            project.Id = id;

            projectRepository.Update(project);
            unitOfWork.SaveChanges();
        }
    }
}
