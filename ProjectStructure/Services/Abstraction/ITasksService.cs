﻿using System.Collections.Generic;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.Models;

namespace ProjectStructure.Services.Abstraction
{
    public interface ITasksService
    {
        List<Task> GetTasks();
        Task GetTaskId(int id);
        List<Task> GetNoFinishedTasks(int id);
        Task CreateTask(TaskDto taskDto);
        void DeleteTask(int id);
        void UpdateTask(int id, TaskDto taskDto);
    }
}
