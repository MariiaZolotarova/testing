﻿using System.Collections.Generic;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.Models;

namespace ProjectStructure.Services.Abstraction
{
    public interface IUsersService
    {
        List<User> GetUsers();
        User GetUsersId(int id);
        User CreateUser(UserDto userDto);
        void DeleteUser(int id);
        void UpdateUser(int id, UserDto userDto);
    }
}
