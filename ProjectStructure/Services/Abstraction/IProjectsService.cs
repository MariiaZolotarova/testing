﻿using System.Collections.Generic;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.Models;

namespace ProjectStructure.Services.Abstraction
{
    public interface IProjectsService
    {
        List<Project> GetProjects();
        Project GetProjectId(int id);
        Project CreateProject(ProjectDto projectDto);
        void DeleteProject(int id);
        void UpdateProject(int id, ProjectDto projectDto);
    }
}
