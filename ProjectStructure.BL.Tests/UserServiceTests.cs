using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FakeItEasy;
using Microsoft.AspNetCore.SignalR;
using ProjectStructure.BL.Context;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.BL.UnitOfWork;
using ProjectStructure.Mapping;
using ProjectStructure.Models;
using ProjectStructure.Services.Implementation;
using ProjectStructure.UnitTests.Helpers;
using Xunit;

namespace ProjectStructure.UnitTests
{
    public class UserServiceTests
    {
        private readonly IMapper _mapper;

        public UserServiceTests()
        {
            var mappingProfile = new MappingProfile();
            var mapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(mappingProfile);
            });

            _mapper = new Mapper(mapperConfiguration);
        }

        [Fact]
        public void CreateUser_WhenCreateUser_ThenUserPlusOne()
        {
            // Arrange
            var context = DbContextHelper.GetProjectContext("1");
            var unitOfWork = new BL.UnitOfWork.UnitOfWork(context);
            var usersService = new UsersService(unitOfWork, _mapper);
            var birthDay = DateTime.UtcNow;
            var userDto = new UserDto
            {
                FirstName = "Abc",
                LastName = "Abc",
                Birthday = birthDay,
                Email = "asdsa",
                TeamId = null
            };

            // Act
            usersService.CreateUser(userDto);

            // Assert
            Assert.Equal(1, context.Users.Count());
            Assert.Equal(userDto.FirstName, context.Users.First().FirstName);
            Assert.Equal(userDto.TeamId, context.Users.First().TeamId);
            Assert.Equal(birthDay, context.Users.First().Birthday);
        }

        [Fact]
        public void CreateUser_WhenCreateUserWithNullName_ThenThrowException()
        {
            // Arrange
            var context = DbContextHelper.GetProjectContext("2");
            var unitOfWork = new BL.UnitOfWork.UnitOfWork(context);
            var usersService = new UsersService(unitOfWork, _mapper);
            var birthDay = DateTime.UtcNow;
            var userDto = new UserDto
            {
                FirstName = null,
                LastName = "Abc",
                Birthday = birthDay,
                Email = "asdsa",
                TeamId = null
            };

            // Act & Assert
            Assert.Throws<ArgumentNullException>(() => usersService.CreateUser(userDto));
        }

        [Fact]
        public void DeleteUser_WhenDeleteUser_ThenUserMinusOne()
        {
            // Arrange
            var context = DbContextHelper.GetProjectContext("3");
            var unitOfWork = new BL.UnitOfWork.UnitOfWork(context);
            var usersService = new UsersService(unitOfWork, _mapper);
            var birthDay = DateTime.UtcNow;
            var user = new User
            {
                FirstName = "Blopy",
                LastName = "Abc",
                Birthday = birthDay,
                Email = "asdsa",
                TeamId = null,
                Id = 1,
                Phone = "666-777",
                RegisteredAt = DateTime.UtcNow
            };
            context.Users.Add(user);
            context.SaveChanges();

            // Act
            int userId = 1;
            usersService.DeleteUser(userId);

            // Assert
            Assert.Equal(0, context.Users.Count());
        }

        [Fact]
        public void GetUsers_GetUserById()
        {
            // Arrange
            var unitOfWork = A.Fake<IUnitOfWork>();
            var usersService = new UsersService(unitOfWork, _mapper);
            var birthDay = DateTime.UtcNow;
            var userDto = new UserDto
            {
                FirstName = "Abc",
                LastName = "Abc",
                Birthday = birthDay,
                Email = "asdsa",
                TeamId = null
            };

            // Act
            usersService.CreateUser(userDto);

            // Assert
            A.CallTo(() => unitOfWork.Set<User>()).MustHaveHappened();
            A.CallTo(() => unitOfWork.SaveChanges()).MustHaveHappened();
        }
    }
}
