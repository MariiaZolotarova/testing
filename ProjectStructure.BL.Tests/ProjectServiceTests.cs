﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using ProjectStructure.Mapping;
using ProjectStructure.Models;
using ProjectStructure.Services.Implementation;
using ProjectStructure.UnitTests.Helpers;
using Xunit;

namespace ProjectStructure.UnitTests
{
    public class ProjectServiceTests
    {
        private readonly IMapper _mapper;

        public ProjectServiceTests()
        {
            var mappingProfile = new MappingProfile();
            var mapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(mappingProfile);
            });

            _mapper = new Mapper(mapperConfiguration);
        }

        [Fact]
        public void CreateProject_WhenCreateProject_ThenProjectPlusOne()
        {
            // Arrange
            var context = DbContextHelper.GetProjectContext("6");
            var unitOfWork = new BL.UnitOfWork.UnitOfWork(context);
            var projectService = new ProjectsService(unitOfWork, _mapper);

            var projectDto = new ProjectDto
            {
                TeamId = 1,
                Description = "description",
                AuthorId = 1,
                DeadLine = DateTime.UtcNow,
                Name = "Bobby's project"
            };
            // Act
            projectService.CreateProject(projectDto);

            // Assert
            Assert.Equal(1, context.Projects.Count());
        }

        [Fact]
        public void CreateProject_WhenCreateProjectWithNullName_ThenThrowException()
        {
            // Arrange
            var context = DbContextHelper.GetProjectContext("7");
            var unitOfWork = new BL.UnitOfWork.UnitOfWork(context);
            var projectService = new ProjectsService(unitOfWork, _mapper);
            var projectDto = new ProjectDto
            {
                TeamId = 1,
                Description = "description",
                AuthorId = 1,
                DeadLine = DateTime.UtcNow,
                Name = null
            };

            // Act & Assert
            Assert.Throws<ArgumentNullException>(() => projectService.CreateProject(projectDto));
        }
    }
}
