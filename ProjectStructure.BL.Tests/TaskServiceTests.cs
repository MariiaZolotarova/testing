﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.Mapping;
using ProjectStructure.Models;
using ProjectStructure.Services.Implementation;
using ProjectStructure.UnitTests.Helpers;
using Xunit;

namespace ProjectStructure.UnitTests
{
    public class TaskServiceTests
    {
        private readonly IMapper _mapper;

        public TaskServiceTests()
        {
            var mappingProfile = new MappingProfile();
            var mapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(mappingProfile);
            });

            _mapper = new Mapper(mapperConfiguration);
        }

        [Fact]
        public void ChangeStatusOfTask_WhenChangeStatus_StatusChanged()
        {
            // Arrange
            var context = DbContextHelper.GetProjectContext("5");
            var task = new Task
            {
                Description = "nbgjob",
                Name = "Name",
                PerformerId = 1,
                ProjectId = 1,
                State = StateEnum.Created,
                CreatedAt = DateTime.UtcNow,
                FinishedAt = null,
                Id = 1
            };
            context.Tasks.Add(task);
            context.SaveChanges();
            context.Entry(task).State = EntityState.Detached;

            var taskDto = new TaskDto
            {
                Description = "nbgjob",
                Name = "Name",
                PerformerId = 1,
                ProjectId = 1,
                State = StateEnum.Finished
            };
            var unitOfWork = new BL.UnitOfWork.UnitOfWork(context);
            var tasksService = new TasksService(unitOfWork, _mapper);

            // Act
            tasksService.UpdateTask(task.Id, taskDto);

            // Assert
            Assert.Equal(StateEnum.Finished, context.Tasks.First().State);
            Assert.NotNull(context.Tasks.First().FinishedAt);
        }

        [Theory]
        [InlineData(StateEnum.Created)]
        [InlineData(StateEnum.Finished)]
        public void CreateTask_WhenCreateTask_ThenTaskPlusOne(StateEnum state)
        {
            // Arrange
            var context = DbContextHelper.GetProjectContext($"CreateTask_WhenCreateTask_ThenTaskPlusOne{state.ToString()}");
            var unitOfWork = new BL.UnitOfWork.UnitOfWork(context);
            var tasksService = new TasksService(unitOfWork, _mapper);

            var taskDtoDto = new TaskDto
            {
                Description = "description",
                Name = "Bobby's task",
                ProjectId = 1,
                PerformerId = 1,
                State = state
            };
            // Act
            tasksService.CreateTask(taskDtoDto);

            // Assert
            Assert.Equal(1, context.Tasks.Count());
            Assert.Equal(state, context.Tasks.First().State);

            if (state == StateEnum.Finished)
            {
                Assert.NotNull(context.Tasks.First().FinishedAt);
            }
            else
            {
                Assert.Null(context.Tasks.First().FinishedAt);
            }
        }

        [Fact]
        public void CreateTask_WhenCreateTaskWithNullName_ThenThrowException()
        {
            // Arrange
            var context = DbContextHelper.GetProjectContext("CreateTask_WhenCreateTaskWithNullName_ThenThrowException");
            var unitOfWork = new BL.UnitOfWork.UnitOfWork(context);
            var taskService = new TasksService(unitOfWork, _mapper);
            var taskDto = new TaskDto
            {
                Description = "description",
                Name = null,
                ProjectId = 1,
                PerformerId = 1,
                State = StateEnum.Created
            };

            // Act & Assert
            Assert.Throws<ArgumentNullException>(() => taskService.CreateTask(taskDto));
        }
    }
}
