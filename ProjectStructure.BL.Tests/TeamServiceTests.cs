﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.Mapping;
using ProjectStructure.Models;
using ProjectStructure.Services.Implementation;
using ProjectStructure.UnitTests.Helpers;
using Xunit;

namespace ProjectStructure.UnitTests
{
    public class TeamServiceTests
    {
        private readonly IMapper _mapper;

        public TeamServiceTests()
        {
            var mappingProfile = new MappingProfile();
            var mapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(mappingProfile);
            });

            _mapper = new Mapper(mapperConfiguration);
        }

        [Fact]
        public void AddUserInTeam_WhenAddUserInTeam_ThenUserPlusOne()
        {
            // Arrange
            var context = DbContextHelper.GetProjectContext("4");
            var unitOfWork = new BL.UnitOfWork.UnitOfWork(context);
            var teamService = new TeamsService(unitOfWork, _mapper);
            var userService = new UsersService(unitOfWork, _mapper);
            var teamDto = new TeamDto
            {
                Name = "DreamTeam"
            };
            var userDto = new UserDto
            {
                Birthday = DateTime.UtcNow,
                TeamId = 1,
                LastName = "Bobby",
                FirstName = "Bob",
                Email = "Bobby@mail.com"
            };

            // Act
            teamService.CreateTeam(teamDto);
            userService.CreateUser(userDto);

            // Assert
            var teamInDb = context.Teams.First();
            Assert.Equal(teamDto.Name, teamInDb.Name);
            Assert.Single(teamInDb.Users);
            Assert.Equal(userDto.FirstName, teamInDb.Users.First().FirstName);
        }
    }
}
