﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using ProjectStructure.BL.Context;

namespace ProjectStructure.BL.Migrations
{
    [DbContext(typeof(ProjectContext))]
    [Migration("20200715170050_AddSeedData")]
    partial class AddSeedData
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.5")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("ProjectStructure.BL.Context.Entity.Project", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("AuthorId")
                        .HasColumnType("int");

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("datetime2");

                    b.Property<DateTime>("DeadLine")
                        .HasColumnType("datetime2");

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("TeamId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("AuthorId");

                    b.HasIndex("TeamId");

                    b.ToTable("Projects");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            AuthorId = 1,
                            CreatedAt = new DateTime(2020, 7, 15, 17, 0, 49, 813, DateTimeKind.Utc).AddTicks(6224),
                            DeadLine = new DateTime(2020, 7, 15, 17, 0, 49, 813, DateTimeKind.Utc).AddTicks(6547),
                            Description = "The best project ever",
                            Name = "Super Project",
                            TeamId = 1
                        },
                        new
                        {
                            Id = 2,
                            AuthorId = 2,
                            CreatedAt = new DateTime(2020, 7, 15, 17, 0, 49, 813, DateTimeKind.Utc).AddTicks(7794),
                            DeadLine = new DateTime(2020, 7, 15, 17, 0, 49, 813, DateTimeKind.Utc).AddTicks(7801),
                            Description = "The worst project ever",
                            Name = "Bad Project",
                            TeamId = 2
                        });
                });

            modelBuilder.Entity("ProjectStructure.BL.Context.Entity.Task", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime?>("FinishedAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("PerformerId")
                        .HasColumnType("int");

                    b.Property<int>("ProjectId")
                        .HasColumnType("int");

                    b.Property<int>("State")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("PerformerId");

                    b.HasIndex("ProjectId");

                    b.ToTable("Tasks");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            CreatedAt = new DateTime(2020, 7, 15, 17, 0, 49, 814, DateTimeKind.Utc).AddTicks(1118),
                            Description = "The best task ever",
                            FinishedAt = new DateTime(2020, 7, 15, 17, 0, 49, 814, DateTimeKind.Utc).AddTicks(1411),
                            Name = "Super task",
                            PerformerId = 1,
                            ProjectId = 1,
                            State = 0
                        },
                        new
                        {
                            Id = 2,
                            CreatedAt = new DateTime(2020, 7, 15, 17, 0, 49, 814, DateTimeKind.Utc).AddTicks(2610),
                            Description = "The hardest task ever",
                            FinishedAt = new DateTime(2020, 7, 15, 17, 0, 49, 814, DateTimeKind.Utc).AddTicks(2617),
                            Name = "Hard task",
                            PerformerId = 2,
                            ProjectId = 2,
                            State = 0
                        });
                });

            modelBuilder.Entity("ProjectStructure.BL.Context.Entity.Team", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Teams");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            CreatedAt = new DateTime(2020, 7, 15, 17, 0, 49, 814, DateTimeKind.Utc).AddTicks(3414),
                            Name = "Best team"
                        },
                        new
                        {
                            Id = 2,
                            CreatedAt = new DateTime(2020, 7, 15, 17, 0, 49, 814, DateTimeKind.Utc).AddTicks(3702),
                            Name = "Loser team"
                        });
                });

            modelBuilder.Entity("ProjectStructure.BL.Context.Entity.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("Birthday")
                        .HasColumnType("datetime2");

                    b.Property<string>("Email")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("FirstName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("LastName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Phone")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("RegisteredAt")
                        .HasColumnType("datetime2");

                    b.Property<int?>("TeamId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("TeamId");

                    b.ToTable("Users");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Birthday = new DateTime(2020, 7, 15, 17, 0, 49, 813, DateTimeKind.Utc).AddTicks(8702),
                            Email = "email@gmail.com",
                            FirstName = "Jhon",
                            LastName = "Labovski",
                            Phone = "111",
                            RegisteredAt = new DateTime(2020, 7, 15, 17, 0, 49, 813, DateTimeKind.Utc).AddTicks(9603),
                            TeamId = 1
                        },
                        new
                        {
                            Id = 2,
                            Birthday = new DateTime(2020, 7, 15, 17, 0, 49, 814, DateTimeKind.Utc).AddTicks(102),
                            Email = "email1@gmail.com",
                            FirstName = "Will",
                            LastName = "Deapy",
                            Phone = "222",
                            RegisteredAt = new DateTime(2020, 7, 15, 17, 0, 49, 814, DateTimeKind.Utc).AddTicks(128),
                            TeamId = 2
                        });
                });

            modelBuilder.Entity("ProjectStructure.BL.Context.Entity.Project", b =>
                {
                    b.HasOne("ProjectStructure.BL.Context.Entity.User", "Author")
                        .WithMany("Projects")
                        .HasForeignKey("AuthorId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("ProjectStructure.BL.Context.Entity.Team", "Team")
                        .WithMany("Projects")
                        .HasForeignKey("TeamId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("ProjectStructure.BL.Context.Entity.Task", b =>
                {
                    b.HasOne("ProjectStructure.BL.Context.Entity.User", "Performer")
                        .WithMany("Tasks")
                        .HasForeignKey("PerformerId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("ProjectStructure.BL.Context.Entity.Project", "Project")
                        .WithMany("Tasks")
                        .HasForeignKey("ProjectId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("ProjectStructure.BL.Context.Entity.User", b =>
                {
                    b.HasOne("ProjectStructure.BL.Context.Entity.Team", "Team")
                        .WithMany("Users")
                        .HasForeignKey("TeamId");
                });
#pragma warning restore 612, 618
        }
    }
}
