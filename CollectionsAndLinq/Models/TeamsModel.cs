﻿using System;
using System.Collections.Generic;

namespace CollectionsAndLinq.Models
{
    public class TeamsModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<UsersModel> Users { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
