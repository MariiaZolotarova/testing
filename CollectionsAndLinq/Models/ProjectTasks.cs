﻿namespace CollectionsAndLinq.Models
{
    public class ProjectTasks
    {
        public ProjectModel Project { get; set; }
        public int TasksCount { get; set; }
        public int NotFinishedTasks { get; set; }
        public TaskModel LongestTask { get; set; }
        public UsersModel User { get; set; }
    }
}
